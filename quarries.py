"""

Datamodel:
    is a labelled directed graph. Each node and edge may have multiple labels.
    Moreover, each node has exactly one id and no two nodes have the same id.
    Id is displayed inside the node, labels outside. When displaying datamodel
    graphs, we often omit nodes, edges, labels or id-s in which we are not
    interested at that moment.
    
    The datamodel graph for
    - a set of Python objects disjoint with the set of nodes,
    - a function `get_data_node` from the Python objects to nodes, that does not
      map objects with different types to the same node, and
    - a set of accessors
    is a minimal graph satisfying the following:
    - The set of nodes is the range (co-domain) of `get_data_node`.
    - Node `get_data_node(obj)` has id `get_data_node(obj).get_id()`.
    - Node `get_data_node(obj)` has label `l` iff `isinstance(obj, l) == True`.
    - There is an edge from `get_data_node(obj0)` to `get_data_node(obj1)` with
      label `l` if there is an accessor `a` such that
      `obj1 in [x for x in a.access(obj0, l)]`.

Note:
    The purpose of `get_data_node` is grouping of the Python objects into
    equivalent classes, which is represented by one data node. This cannot be
    done by the correspondence functions, because equivalence relation is
    generally not a function.

Note:
    The word "role" is confusing! It is a relation and it is binary, so let's
    use "relation". An object plays a "role" in a "relation". There is a
    relation "isAParentOf", the first elements in its tuples/pairs have the role
    "parent" in this relation and the second elements have the role "child".

Example:
    +---------+------+-------+
    | gene_id | nucl | count |
    +---------+------+-------+
    |       1 |    a |  3256 |
    |       1 |    c | 95051 |
    |       1 |    g |   550 |
    |       1 |    t |  7811 |
    |       2 |    a | 39264 |
    |       2 |    c | 80012 |
    |       2 |    g |  5453 |
    |       2 |    t |   432 |
    |       3 |    a |     1 |
    |     ... |  ... |   ... |

"""



from collections import defaultdict
from collections.abc import Callable, Collection, Hashable, Iterable, Mapping, MutableMapping, Sequence
from dataclasses import dataclass
import io
from typing import Any, TextIO



PRIMITIVE_DATATYPES = frozenset((bool, float, int, str, type, type(None)))

NODE_LABEL2TYPE: MutableMapping[str, type] = {
    "None": type(None),
    "bool": bool,
    "dict": dict,
    "float": float,
    "frozenset": frozenset,
    "int": int,
    "list": list,
    "set": set,
    "str": str,
    "type": type,
}




class Accessor:# TODO: try ABC ??
    '''
    Accesses attributes of an object. If two objects are in relation `r`,
    an accessor can access the second object from the first one via `r`.

    The simplest example is the `MappingAccessor`, where the first object `obj0`
    is a `Mapping` and the second object `obj1` is in `obj0.values()`. These two
    objects are in a relation that has name `key` with `obj0[key] == obj1`.

    An object may be in the same relation with a multiple other objects, so
    `access` returns an `Iterable`.
    '''

    def is_in_relation(self, obj, relation_name: str) -> bool:
        raise NotImplementedError

    def access(self, obj, relation_name: str) -> Iterable:
        raise NotImplementedError

class RelationIteratingAccessor(Accessor):# TODO: try ABC ??

    def all_relation_names(self, obj) -> Iterable[str]:
        raise NotImplementedError



class MappingAccessor(RelationIteratingAccessor):
    '''
    The `relation_name` is a key in the mapping `obj`, so the related/accessed
    object is `obj[relation_name]`.

    Note that keys of mapping `obj` that are not a `str` are ignored (there are
    no non-`str` role names).
    '''

    def is_in_relation(self, obj, relation_name: str) -> bool:
        return isinstance(obj, Mapping) and relation_name in obj

    def access(self, obj, relation_name: str) -> Iterable:
        return (obj[relation_name], )

    def all_relation_names(self, obj) -> Iterable[str]:
        if not isinstance(obj, Mapping):
            return ()
        # else:
        return (key for key in obj.keys() if isinstance(key, str))

MAPPING_ACCESSOR = MappingAccessor()



class VarsAccessor(MappingAccessor):
    '''
    Most straightforward accessor. Relations are attributes relating `obj` with
    the values of the attributes. Technically, related objects are
    `MappingAccessor().access(vars(obj), relation_name)`.
    '''

    def is_in_relation(self, obj, relation_name: str) -> bool:
        return hasattr(obj, '__dict__') and super().is_in_relation(vars(obj), relation_name)

    def access(self, obj, relation_name: str) -> Iterable:
        return super().access(vars(obj), relation_name)

    def all_relation_names(self, obj) -> Iterable:
        if not hasattr(obj, '__dict__'):
            return ()
        # else:
        return super().all_relation_names(vars(obj))

VARS_ACCESSOR = VarsAccessor()



class PublicVarsAccessor(VarsAccessor):
    "a `VarsAccessor` limited to public attributes."

    def is_in_relation(self, obj, relation_name: str) -> bool:
        return not relation_name.startswith('_') and super().is_in_relation(obj, relation_name)

    def all_relation_names(self, obj) -> Iterable:
        return (rel for rel in super().all_relation_names(obj) if not rel.startswith('_'))

PUBLIC_VARS_ACCESSOR = PublicVarsAccessor()



class IterableAccessor(RelationIteratingAccessor):
    """
    The only relation name is `"__member__"` and the related objects are the
    members of the Iterable `obj`.
    """

    def is_in_relation(self, obj, relation_name: str) -> bool:
        return relation_name == '__member__' and isinstance(obj, Iterable)

    def access(self, obj, relation_name: str) -> Iterable:
        # if isinstance(obj, Iterable):
        return obj

    def all_relation_names(self, obj) -> Iterable[str]:
        if not isinstance(obj, Iterable):
            return ()
        # else:
        return ("__member__", )

ITERABLE_ACCESSOR = IterableAccessor()



class SequenceAccessor(RelationIteratingAccessor):
    """
    Relation names look like `"__42__"` and the only object related with this
    relation is `obj[42]`.
    """
    
    def _parse_relation_name(self, relation_name: str) -> int:
        if not (relation_name.startswith('__') and relation_name.endswith('__')):
            raise ValueError(f'relation_name "{relation_name}" is not of the form "__{{index}}__"!')
        # else:
        try:
            index = int(relation_name[2:-2])
        except ValueError:
            raise ValueError(f'relation_name "{relation_name}" is not of the form "__{{index}}__"!')
        return index

    def is_in_relation(self, obj, relation_name: str) -> bool:
        if not isinstance(obj, Sequence):
            return False
        # else:
        try:
            index = self._parse_relation_name(relation_name)
        except ValueError:
            return False
        return 0 <= index and index < len(obj)

    def access(self, obj, relation_name: str) -> Iterable:
        return (obj[self._parse_relation_name(relation_name)], )

    def all_relation_names(self, obj) -> Iterable[str]:
        if not isinstance(obj, Sequence):
            return ()
        # else:
        return (f"__{index}__" for index in range(len(obj)))

SEQUENCE_ACCESSOR = SequenceAccessor()



DEFAULT_ACCESSORS: tuple[RelationIteratingAccessor] = (
    ITERABLE_ACCESSOR,
    MAPPING_ACCESSOR,
    PUBLIC_VARS_ACCESSOR,
    SEQUENCE_ACCESSOR,
)



class DataNode:# TODO: try ABC ??
    
    def get_obj(self):
        raise NotImplementedError
    
    def get_id(self) -> Hashable:
        raise NotImplementedError
    
    def get_type(self) -> type:
        raise NotImplementedError

@dataclass(frozen=True)
class SimpleDataNode(DataNode):
    obj: Any
    
    def get_obj(self):
        return self.obj
    
    def get_id(self) -> Hashable:
        return id(self.obj)
    
    def get_type(self) -> type:
        return type(self.obj)

DEFAULT_GET_DATA_NODE = SimpleDataNode



def collect_datamodel_graph(
    nodes: MutableMapping[Hashable, DataNode],
    edges: MutableMapping[tuple[Hashable, Hashable], set[str]],
    root,
    max_depth: int=5,
    get_data_node: Callable[[Any], DataNode]=DEFAULT_GET_DATA_NODE,
    accessors: Iterable[RelationIteratingAccessor]=DEFAULT_ACCESSORS,
):
    node = get_data_node(root)
    nodes[node.get_id()] = node
    
    if max_depth <= 0:
        return
    # else:
    for accessor in accessors:
        for relation_name in accessor.all_relation_names(root):
            for next_obj in accessor.access(root, relation_name):
                edge = (node.get_id(), get_data_node(next_obj).get_id())
                
                rels = edges.get(edge, set())
                if not rels:
                    edges[edge] = rels
                rels.add(relation_name)
                
                collect_datamodel_graph(
                    nodes=nodes,
                    edges=edges,
                    root=next_obj,
                    max_depth=max_depth-1,
                    get_data_node=get_data_node,
                    accessors=accessors,
                )



def datamodel2dot(
    output: TextIO,
    root,
    max_depth: int=5,
    get_data_node: Callable[[Any], DataNode]=DEFAULT_GET_DATA_NODE,
    accessors: Iterable[RelationIteratingAccessor]=DEFAULT_ACCESSORS,
) -> None:
    """
    Writes the datamodel reachable from `obj` in at most `max_depth` steps to
    `output` in the DOT format.
    
    In Python it is not possible to enumerate all `l` such that
    `isinstance(obj, l) == True` for a fiven `obj`. So only `type(obj)` is
    written as a label.
    """
    
    nodes = {}
    edges = {}
    collect_datamodel_graph(
        nodes, edges, root, max_depth, get_data_node, accessors
    )
    
    print("digraph {", file=output)
    print("rankdir=LR;", file=output)
    print("nodesep=0.9;", file=output)
    print("ranksep=0.9;", file=output)
    
    for node_id, node in nodes.items():
        label = str(node_id)
        shape = 'ellipse'
        if node.get_type() in PRIMITIVE_DATATYPES:
            label += fr'\n{node.get_obj()!r}'
            shape = 'box'
        xlabel = node.get_type().__name__
        print(f'"{str(node_id)}" [shape="{shape}" label="{label}" xlabel="{xlabel}"];', file=output)
    
    for (node0, node1), rels in edges.items():
        labels = r"\n".join(sorted(rels))
        print(f'"{str(node0)}" -> "{str(node1)}" [label="{labels}"];', file=output)
    
    print("}", file=output)



@dataclass(frozen=True)
class QueryNode:
    var_name: str
    class_name: str # TODO: rename to node_label
    edges: tuple['QueryEdge', ...]
    
    def write_datalog(self, output: TextIO, indent: int=0):
        
        class_str = f'{self.class_name}({self.var_name}), '
        print(class_str, file=output, end='')
        prop_indent = indent + len(class_str)
        
        is_first = True
        for prop in self.edges:
            prop_str = f'{prop.role_name}({self.var_name},{prop.node.var_name}), '
            if not is_first:
                print(' ' * prop_indent, file=output, end='')
            print(prop_str, file=output, end='')
            prop.node.write_datalog(output, prop_indent + len(prop_str))
            if is_first:
                is_first = False
        
        print(file=output)
    
    def str_datalog(self) -> str:
        output = io.StringIO()
        self.write_datalog(output)
        return output.getvalue()

@dataclass(frozen=True)
class QueryEdge:
    role_name: str # TODO: rename to edge_label
    node: QueryNode



@dataclass(frozen=True)
class ExplicitIdDataNode(SimpleDataNode):
    node_id: Hashable
    
    def get_id(self) -> Hashable:
        return self.node_id

class IntIdGetDataNode:
    
    def __init__(self):
        self.obj_id2node = {}
    
    def __call__(self, obj) -> DataNode:
        node = self.obj_id2node.get(id(obj), None)
        if node is None:
            node = ExplicitIdDataNode(obj=obj, node_id=len(self.obj_id2node))
            self.obj_id2node[id(obj)] = node
        return node



def resolve_type(node_label) -> type:
    result = NODE_LABEL2TYPE.get(node_label)
    if result is not None:
        return result
    # else: Try to import node_label.
    import_path = node_label.split('.')
    import_path.reverse()
    result = __import__(import_path.pop())
    while import_path:
        result = getattr(result, import_path.pop())
    return result



def cross_product_matches(matches1: Iterable[Mapping[str, DataNode]], matches2: Iterable[Mapping[str, DataNode]]) -> Iterable[Mapping[str, DataNode]]:
    
    result = []
    for match1 in matches1:
        for match2 in matches2:
            
            # Common variables must match the same data.
            if any(match1[var].get_id() != match2[var].get_id() for var in match1.keys() & match2.keys()):
                continue
            # TODO: define __eq__ and __hash__ on DataNode that uses self.get_id() and just write match1[var] != match2[var]
            
            match = dict(match1)
            match.update(match2)
            result.append(match)
    
    return result



# FUTURE: Match is actually a TypedDict and the types of the values can be read. from the query ;-)
# FUTURE: This is alternation of union and join. It can be implemented as justifications ;-)
def enumerate_query_matches(
    query: QueryNode,
    root,
    get_data_node: Callable[[Any], DataNode]=DEFAULT_GET_DATA_NODE,
    accessors: Iterable[RelationIteratingAccessor]=DEFAULT_ACCESSORS,
) -> Iterable[Mapping[str, DataNode]]:
    
    if not isinstance(root, resolve_type(query.class_name)):
        # The node label does not match.
        return []
    # else: The node label matches.
    
    data_node = get_data_node(root)
    matches = [{query.var_name: data_node}]
    
    # Matches produced by one edge need to be joined with matches produced by the other. (cartesian product)
    for query_edge in query.edges:
        
        # Each accessor produces an alternative match.
        accessor_matches = []
        for accessor in accessors:
            if accessor.is_in_relation(root, query_edge.role_name):
                for next_obj in accessor.access(root, query_edge.role_name):
                    
                    accessor_matches += enumerate_query_matches(
                        query=query_edge.node,
                        root=next_obj,
                        get_data_node=get_data_node,
                        accessors=accessors,
                    )
        
        matches = cross_product_matches(matches, accessor_matches)
    
    return matches



def print_eq(x, x_name: str, same, same_name, other, other_name):
    
    print(f'{x_name}: {x}')
    print(f'id({x_name}): {id(x)}')
    print(f'hash({x_name}): {hash(x)}')
    
    print(f'{same_name}: {same}')
    print(f'id({same_name}): {id(same)}')
    print(f'hash({same_name}): {hash(same)}')
    
    print(f'{other_name}: {other}')
    print(f'id({other_name}): {id(other)}')
    print(f'hash({other_name}): {hash(other)}')
    
    print(f'{x_name} == {x_name}: {x == x}')
    print(f'{x_name} is {x_name}: {x is x}')
    print(f'id({x_name}) == id({x_name}): {id(x) == id(x)}')
    print(f'hash({x_name}) == hash({x_name}): {hash(x) == hash(x)}')
    
    print(f'{x_name} == {same_name}: {x == same}')
    print(f'{x_name} is {same_name}: {x is same}')
    print(f'id({x_name}) == id({same_name}): {id(x) == id(same)}')
    print(f'hash({x_name}) == hash({same_name}): {hash(x) == hash(same)}')
    
    print(f'{x_name} == {other_name}: {x == other}')
    print(f'{x_name} is {other_name}: {x is other}')
    print(f'id({x_name}) == id({other_name}): {id(x) == id(other)}')
    print(f'hash({x_name}) == hash({other_name}): {hash(x) == hash(other)}')



if __name__ == '__main__':
    '''
    n0 = QueryNode('X', 'str', ())
    n1 = QueryNode('X', 'str', ())
    n2 = QueryNode('Y', 'str', ())
    print()
    print_eq(n0, 'n0', n1, 'n1', n2, 'n2')
    
    p0 = QueryEdge('field1', n0)
    p1 = QueryEdge('field1', n1)
    p2 = QueryEdge('field1', n2)
    print()
    print_eq(p0, 'p0', p1, 'p1', p2, 'p2')
    '''
    
    q0 = QueryNode(
        'Root',
        'root',
        (
            QueryEdge(
                'cons_adding',
                QueryNode('C_adding', 'str', ()),
            ),
            QueryEdge(
                'desired_impact',
                QueryNode(
                    'Row',
                    'desired_impact_df_row',
                    (
                        QueryEdge(
                            'eqp',
                            QueryNode('EL', 'eqpL', ()),
                        ),
                        QueryEdge(
                            'type',
                            QueryNode('C_adding', 'str', ()),
                        ),
                        QueryEdge(
                            'impact',
                            QueryNode('I', 'int', ()),
                        ),
                    ),
                ),
            ),
        ),
    )
    '''
    print()
    print()
    print("q0.str_datalog():")
    print()
    print(q0.str_datalog())
    
    
    output = io.StringIO()
    datamodel2dot(
        output,
        root={
            'data': [
                {
                    'name': 'Joseph',
                    'age': 12,
                },
                {
                    'name': 'John',
                    'age': -42,
                    'null': None,
                },
            ],
#            'query': q0,
        },
        max_depth=15,
        get_data_node=IntIdGetDataNode(),
    )
    print(output.getvalue())
    
    
    n0 = ExplicitIdDataNode('X', node_id=1)
    n1 = ExplicitIdDataNode('X', node_id=1)
    n2 = ExplicitIdDataNode('Y', node_id=1)
    print()
    print_eq(n0, 'n0', n1, 'n1', n2, 'n2')
    print()
    print(n0)
    print(n0.get_obj())
    print(n0.get_id())
    print(n0.get_type())
    print(isinstance(n0, DataNode))
    
    print(dir(n0))
    print(dir(n0.__hash__))
    print(dir(n0.__hash__.__func__))
    print(n0.__hash__.__func__.__code__)
    print(dir(n0.__hash__.__func__.__code__))
    print(repr(n0.__hash__.__func__.__code__))
    
    S = set()
    S.add(n0)
    print(S)
    '''
    
    root={
        'const_age': 12,
        'data': [
            {
                'name': 'Joseph',
                'age': 12,
            },
            {
                'name': 'John',
                'age': -42,
                'null': None,
            },
        ],
    }
    q1 = QueryNode(
        'Root',
        'dict',
        (
#            QueryEdge(
#                'const_age',
#                QueryNode('Age', 'int', ()),
#            ),
            QueryEdge(
                'data',
                QueryNode(
                    'DataList',
                    'list',
                    (
                        QueryEdge(
                            '__member__',
                            QueryNode(
                                'DataListMember',
                                'dict',
                                (
                                    QueryEdge(
                                        'name',
                                        QueryNode('Name', 'str', ()),
                                    ),
                                    QueryEdge(
                                        'age',
                                        QueryNode('Age', 'int', ()),
                                    ),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        ),
    )
    
    import pprint
    print()
    print()
    print('pprint.pprint(root):')
    print()
    pprint.pprint(root)
    print()
    print()
    print("q1.str_datalog():")
    print()
    print(q1.str_datalog())
    
    matches = enumerate_query_matches(q1, root, get_data_node=IntIdGetDataNode())
    pprint.pprint(matches)



