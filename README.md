

# Quarries

Schema transformations

That is too general, I know. But the idea is pretty general too.
I'll try to do non-recursive datalog over graph data with
user-guided existential quantification. But let's see where this ends up :-P

Dreams:
- Translate data from one schema to another.
- Support multiple data formats
  (Python objects, SQL, First-order structures, JSON, graph databases, ...,
  can we do anything people call "data"??)
- Some automated reasoning
  (prove that if one constraint holds before the translation
  then another one holds after)
- ...


## Example

(All names and numbers are made up.
This example may not make sense in the domain of bioinformatics,
but I worked with real data that was isomorphic to this.)

Imagine that one bioinformatic algorithm outputs data with schema *S1* and
we want to feed this data to another bioinformatic algorithm that, however,
accepts data only in schema *S2*.

All names in *S1* end with `_1` and all names in *S2* end with `_2`.


### First About Schema *S1*

Schema *S1* has a format of a relational database.
There is a table of all genes (present in the output of the first algorithm).
It may have a whole bunch of data about each gene, but
that is not relevant for this example.
The only thing relevant here is that ID-s of all genes are in this table.

**Table `gene_1`**
| `gene_id_1` |
| ---:        |
| 1           |
| 2           |
| 3           |
| ...         |

Then there is a separate table with how many bases each gene contains:

**Table `base_count_1`**
| `gene_id_1` | `base_1` | `count_1` |
| ---:        | ---      | ---:      |
|           1 |        a |      3256 |
|           1 |        c |     95051 |
|           1 |        g |       550 |
|           1 |        t |      7811 |
|           2 |        a |     39264 |
|           2 |        c |     80012 |
|           2 |        g |      5453 |
|           2 |        t |       432 |
|           3 |        a |         1 |
|         ... |  ...     |       ... |

Now this is asking for the constraint that we have count of each base
in each gene.
This constraint is too expressive for commonly used systems.
But we can simplify it, because there is exactly 4 bases.
So in this example we use 4 constraints, one for each base:
```math
\forall G . \mathsf{gene\_1}(G) \rightarrow
\exists C . \mathsf{base\_count\_1}(G, \mathsf{a}, C)
```

Practically, there would be much more different kinds of data in *S1*, but
in this example we focus only on the tables above.

### Now About Schema *S2*

Schema *S2* has a format of a set of logic programming (ASP) facts with
arity at most 2 (and at least 1).
So we can also see it as graph data:
the unary predicates are node labels and
the binary predicates are edge labels.

**Predicate `gene_2(G)`** means that `G` is a gene. The set of all genes in the data.\
`gene_2(1).` `gene_2(2).` `gene_2(3).` ...

**Predicate `base_a_count_2(G,C)`** means that gene `G` contains `C` bases A.\
`base_a_count_2(1,3256).` `base_a_count_2(2,39264).` `base_a_count_2(3,1).` ...

Now the constraint above can be expressed in a light-weight description logic.
But we write it in the same language: first order logic:
```math
\forall G . \mathsf{gene\_2}(G) \rightarrow
\exists C . \mathsf{base\_a\_count\_2}(G, C)
```

>>>
Note: There are also constraints that are "more expressive" in *S2* than in *S1*.
For example, functionality of `base_a_count_2` is
uniquenes in `base_count_1` over columns `gene_id_1` and `base_1`.
>>>

### The Translation Rules

The translation rules have three parts.
The two parts on the top show how the data is translated and
the part on the bottom states the correspondence constraints.
The first rule translates the gene data (in this example only ID-s).

**`rule_gene`**
```
gene_1(G)  |  gene_2(G)
=======================
{}
```

The second rule translates the part of the count for the base A.

**`rule_base_a_count`**
```
base_count_1(G,a,C)  |  base_a_count_2(G,C)
===========================================
{}
```

Unfortunatelly, this example is too simple and no correspondence constraints are needed :-P
So the third, bottom part of the translation rules is empty (denoted with `{}`).
We'll go through an example with correspondence constraints in a bit.

The rules must work in both directions:
from left to right (from *S1* to *S2*) and also
from right to left (from *S2* to *S1*).
When a direction is chosen, the rules work like Datalog rules
(while satisfying the correspondence constraints).

**Datalog for direction L2R of `rule_gene`**
```
gene_2(G) :- gene_1(G).
```
```math
\forall G . \mathsf{gene\_1}(G) \rightarrow \mathsf{gene\_2}(G)
```

**Datalog for direction L2R of `rule_base_a_count`**
```
base_a_count_2(G,C) :- base_count_1(G,a,C).
```
```math
\forall G . \forall C . \mathsf{base\_count\_1}(G, \mathsf{a}, C) \rightarrow 
\mathsf{base\_a\_count\_2}(G, C)
```

**Datalog for direction R2L of `rule_gene`**
```
gene_1(G) :- gene_2(G).
```
```math
\forall G . \mathsf{gene\_2}(G) \rightarrow \mathsf{gene\_1}(G)
```

**Datalog for direction R2L of `rule_base_a_count`**
```
base_count_1(G,a,C) :- base_a_count_2(G,C).
```
```math
\forall G . \forall C . \mathsf{base\_a\_count\_2}(G, C) \rightarrow 
\mathsf{base\_count\_1}(G, \mathsf{a}, C)
```

For Datalog rules to work, each variable in head must occur in the body.
As we want translation in both dirrections, each variable on either side must occur on the other side,
**or it is bound to some variable on the other side with a correspondence constraint** ;-)

So now the example that requires correspondence constraints.


## More Interesting Example

Imagine that somebody has already translated *S1* into logic programming facts with maximal arity 2 (graph data),
but they did not do anything clever:
Each row in each table gets a new object and
the values in this row are related to this object with a binary relation for the corresponding column.
This gives rise to the schema *S3*:

**Predicate `gene_3(R)`** means that `R` is a row in the table `gene_1`.
```
gene_3(gene_row_1).
gene_3(gene_row_2).
gene_3(gene_row_3).
...
```

**Predicate `gene__gene_id_3(R,G)`** means that row `R` contains `G` in the column `gene_id_1`.
```
gene__gene_id_3( gene_row_1, 1 ).
gene__gene_id_3( gene_row_2, 2 ).
gene__gene_id_3( gene_row_3, 3 ).
...
```

**Predicate `base_count_3(R)`** means that `R` is a row in the table `base_count_1`.
```
base_count_3(base_count_row_1).
base_count_3(base_count_row_2).
base_count_3(base_count_row_3).
...
```

**Predicate `base_count__gene_id_3(R,G)`** means that row `R` contains `G` in the column `gene_id_1`.
```
base_count__gene_id_3( base_count_row_1, 1 ).
base_count__gene_id_3( base_count_row_2, 1 ).
base_count__gene_id_3( base_count_row_3, 1 ).
base_count__gene_id_3( base_count_row_4, 1 ).
base_count__gene_id_3( base_count_row_5, 2 ).
...
```

**Predicate `base_count__base_3(R,B)`** means that row `R` contains `B` in the column `base_1`.
```
base_count__base_3( base_count_row_1, a ).
base_count__base_3( base_count_row_2, c ).
base_count__base_3( base_count_row_3, g ).
base_count__base_3( base_count_row_4, t ).
base_count__base_3( base_count_row_5, a ).
base_count__base_3( base_count_row_6, c ).
base_count__base_3( base_count_row_7, g ).
base_count__base_3( base_count_row_8, t ).
base_count__base_3( base_count_row_9, a ).
...
```

**Predicate `base_count__count_3(R,C)`** means that row `R` contains `C` in the column `count_1`.
```
base_count__count_3( base_count_row_1,  3256 ).
base_count__count_3( base_count_row_2, 95051 ).
base_count__count_3( base_count_row_3,   550 ).
base_count__count_3( base_count_row_4,  7811 ).
base_count__count_3( base_count_row_5, 39264 ).
base_count__count_3( base_count_row_6, 80012 ).
base_count__count_3( base_count_row_7,  5453 ).
base_count__count_3( base_count_row_8,   432 ).
base_count__count_3( base_count_row_9,     1 ).
...
```

The constraint is now:
```math
\forall X . \forall G . \mathsf{gene\_\_gene\_id\_3}(X,G) \rightarrow
\exists R . \exists C . \mathsf{base\_count\_\_gene\_id\_3}(R, G) \wedge
\mathsf{base\_count\_\_base\_3}(R, \mathsf{a}) \wedge
\mathsf{base\_count\_\_count\_3}(R, C)
```

### The Translation Rules with Correspondence Constraints

**`rule_gene`**
```
gene_3(R)             |  gene_2(G)
gene__gene_id_3(R,G)  |  
==================================
corr_gene3row(R,G)
```

**`rule_base_a_count`**
```
base_count_3(R)             |  base_a_count_2(G,C)
base_count__gene_id_3(R,G)  |  
base_count__base_3(R,a)     |  
base_count__count_3(R,C)    |  
==================================================
corr_basecount3row(R,G)
```

When translating from right to left, we have a problem:
We don't know what row object to use.
Should we always pick a fresh object?
Should we reuse some existing object?
If yes, which one?

Correspondence constraints tell us when we cannot use fresh object and which existing object to use.
They are essentially binary predicates that are functional in both directions. In other word, bijections.
For example, the correspondence constraint `corr_gene3row` says that
a gene ID corresponds to the object of a row in table `gene_1`.
This is because `gene_id_1` is primary key to the table `gene_1` (should have said that before :-P).
So, obviously, there is a bijection between the rows of table `gene_1` and gene ID-s.
We call this bijection `corr_gene3row`.

We use correspondence constraints also as predicates and also as functions.
So for every correspondence constraint *corr*, we have:
```math
\forall X . \forall Y . corr(X,Y) \leftrightarrow corr(X)=Y
```
```math
\forall X . \forall Y . corr(X,Y) \leftrightarrow X=corr^{-}(Y)
```
And, of course, both directional functionality (or does this follow form the above?):
```math
\forall X . \forall Y . \forall Z . corr(X,Y) \wedge corr(X,Z) \rightarrow Y=Z
```
```math
\forall X . \forall Y . \forall Z . corr(X,Y) \wedge corr(Z,Y) \rightarrow X=Z
```

### The Translation Rules as Datalog

**Datalog for direction L2R of `rule_gene`**
```
gene_2(G) :- gene_3(R), gene__gene_id_3(R,G).
```
```math
\forall G . \mathsf{gene\_3}(R) \wedge \mathsf{gene\_\_gene\_id\_3}(R, G) \rightarrow \mathsf{gene\_2}(G)
```

**Datalog for direction L2R of `rule_base_a_count`**
```
base_a_count_2(G,C) :- base_count_3(R), base_count__gene_id_3(R,G), base_count__base_3(R,a), base_count__count_3(R,C).
```
```math
\forall G . \mathsf{base\_count\_3}(R) \wedge \mathsf{base\_count\_\_gene\_id\_3}(R, G) \wedge \mathsf{base\_count\_\_base\_3}(R, \mathsf{a}) \wedge \mathsf{base\_count\_\_count\_3}(R, C) \rightarrow \mathsf{base\_a\_count\_2}(G, C)
```

**Datalog for direction R2L of `rule_gene`**
```
gene_3( corr_gene3row-(G) ) :- gene_2(G).
gene__gene_id_3( corr_gene3row-(G), G ) :- gene_2(G).
```
```math
\forall G . \mathsf{gene\_2}(G) \rightarrow \mathsf{gene\_3}(\mathsf{corr\_gene3row}^{-}(G))
```
```math
\forall G . \mathsf{gene\_2}(G) \rightarrow \mathsf{gene\_\_gene\_id\_3}(\mathsf{corr\_gene3row}^{-}(G),G)
```

**Datalog for direction R2L of `rule_base_a_count`**
```
base_count_3( corr_basecount3row-(G) ) :- base_a_count_2(G,C).
base_count__gene_id_3( corr_basecount3row-(G), G ) :- base_a_count_2(G,C).
base_count__base_3( corr_basecount3row-(G), a ) :- base_a_count_2(G,C).
base_count__count_3( corr_basecount3row-(G), C ) :- base_a_count_2(G,C).
```
```math
\forall G . \mathsf{base\_a\_count\_2}(G, C) \rightarrow \mathsf{base\_count\_3}(\mathsf{corr\_basecount3row}^{-}(G))
```
```math
\forall G . \mathsf{base\_a\_count\_2}(G, C) \rightarrow \mathsf{base\_count\_\_gene\_id\_3}(\mathsf{corr\_basecount3row}^{-}(G), G)
```
```math
\forall G . \mathsf{base\_a\_count\_2}(G, C) \rightarrow \mathsf{base\_count\_\_base\_3}(\mathsf{corr\_basecount3row}^{-}(G), \mathsf{a})
```
```math
\forall G . \mathsf{base\_a\_count\_2}(G, C) \rightarrow \mathsf{base\_count\_\_count\_3}(\mathsf{corr\_basecount3row}^{-}(G), C)
```

### Example Proof

We would like to have a lot of support (automatize) proving that
a given constraint holds after applying the translation on arbitrary data that satisfy some other constraints.

In the example below we'll show how to prove that the constraint
```math
\forall G . \mathsf{gene\_2}(G) \rightarrow
\exists C . \mathsf{base\_a\_count\_2}(G, C)
```
holds when the translation rules are applied in the direction L2R to any input data that satisfy the constraint 
```math
\forall X . \forall G . \mathsf{gene\_\_gene\_id\_3}(X,G) \rightarrow
\exists R . \exists C . \mathsf{base\_count\_\_gene\_id\_3}(R, G) \wedge
\mathsf{base\_count\_\_base\_3}(R, \mathsf{a}) \wedge
\mathsf{base\_count\_\_count\_3}(R, C)
```
and a number of other integrity constraints listed below.

Applying the Datalog rules for the direction L2R adds new facts (writes new relations).
It does not remove the old ones.
So the data after the translation will consist of the input data and
the data added by the Datalog rules and no other data.
Any such output data must (obviously) satisfy completion of the Datalog rules and
whatever constrants were satisfied by the input data.

The proof will be by contradiction:
We will derive contradiction from the conjunction of
the integrity constraints on the input data,
completion of the Datalog rules and
negated goal constraint.

Let us give labels to all the constraints:



# TODO ...

`goal`:
```math
\forall G . \mathsf{gene\_2}(G) \rightarrow
\exists C . \mathsf{base\_a\_count\_2}(G, C)
```
`rule_gene`:
```math
\forall G . \mathsf{gene\_3}(R) \wedge \mathsf{gene\_\_gene\_id\_3}(R, G) \rightarrow \mathsf{gene\_2}(G)
```
`rule_base_a_count`:
```math
\forall G . \mathsf{base\_count\_3}(R) \wedge \mathsf{base\_count\_\_gene\_id\_3}(R, G) \wedge \mathsf{base\_count\_\_base\_3}(R, \mathsf{a}) \wedge \mathsf{base\_count\_\_count\_3}(R, C) \rightarrow \mathsf{base\_a\_count\_2}(G, C)
```

**TODO:** We want to prove that the second constraint holds whenever the first constraint and the DL-Lite constraints hold

**TODO:** Translation is not reversible (roundtrip does not produce the starting data),
if the right side of some rule is a subset of the right side of some other rule (modulo variable renaming), but
the first rule is not a subrule of the second one (modulo variable renaming).

**TODO:** It is impossible to create data graph that has a cycle over required edges
(all edges in the cycle are for reouired properties, so the object cannot be created without assigning that property).
So we need a constraint asserting that there is no cycle in the data and
a way to prove that this is satisfied in the resulting data if it is satisfied in the input data.
Or restrict the querries so that they can nover match (create, on the other side) a cycle.
E.g., eact variable occurs at most once.

**TODO:** Schema *S3* is basically *S1* in JSON (as would be written by a database cursor)
```json
{
  "gene_3": [
    {"gene_id_3": 1},
    {"gene_id_3": 2},
    {"gene_id_3": 3},
    ...
  ],
  "base_count_3": [
    {"gene_id_3": 1, "base_3": "a", "count_3": 3256},
    {"gene_id_3": 1, "base_3": "c", "count_3": 95051},
    {"gene_id_3": 1, "base_3": "g", "count_3": 550},
    {"gene_id_3": 1, "base_3": "t", "count_3": 7811},
    {"gene_id_3": 2, "base_3": "a", "count_3": 3256},
    {"gene_id_3": 2, "base_3": "c", "count_3": 80012},
    {"gene_id_3": 2, "base_3": "g", "count_3": 5453},
    {"gene_id_3": 2, "base_3": "t", "count_3": 432},
    {"gene_id_3": 3, "base_3": "t", "count_3": 1},
    ...
  ],
  ...
}
```

```
gene_3(data3,L), __member__(L,R), gene_id_3(R,G3)
---------------------------------------------
data(data4), gene_4(G4)
=============================================
corr_gene3list(L,data4)
corr_gene3row(R,G4)
corr_gene(G3,G4)
```

```
base_count_3(data3,L), __member__(L,R), gene_id_3(R,G3), base_3(R,a), count_3(R,C4)
----------------------------------------------------------------------------------
data(data4), base_a_count_2(G4,C4)
==================================================================================
corr_basecount3list(L,data4)
corr_basecount3baseArow(R,G4)
corr_gene(G3,G4)
corr_count(C3,C4)
```



**TODO:** use *S1* translated to graph database :-P



